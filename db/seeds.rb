# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Seed Admin
admin_emails = %w(natalia.m.sergeeva@gmail.com tourrest@mail.ru)
default_password = '123456'

admin_emails.each do |email|
  AdminUser.find_or_create_by_email(email, password: default_password)
end

# Seed Pages
Settings.pages.to_hash.each do |code, name|
  Page.find_or_create_by_code(code, name: name, content: 'Sample text')
end

# Seed Country, Regions, Cities
country = Country.find_or_create_by_name('Россия', code: 'RU', currency_code: 'RUB', visa: Country::VISA_TYPES.first)
[
    {region: 'Московская область', city: 'Москва'},
    {region: 'Республика Башкортостан', city: 'Уфа'},
    {region: 'Пермский край', city: 'Пермь'},
    {region: 'Удмуртия'},
    {region: 'Башкирия'},
    {region: 'Татария'},
    {region: 'Кировская область'},
    {region: 'Свердловская область'},
    {region: 'Челябинская область'},
    {region: 'КавМинВоды'}
].each do |place|
  region = Region.find_or_create_by_name(place[:region], country_id: country.id)
  City.find_or_create_by_name(place[:city], region_id: region.id)
end

# Seed Active Rest
%w(Сплавы Пешие\ походы Конно-верховые\ туры).each do |ar|
  ActiveRest.find_or_create_by_name(ar)
end

# Seed Rest for Children
%w(Из\ Ижевска Из\ Перми Из\ Москвы С\ изучением\ иностранных\ языков).each do |rfc|
  RestForChildren.find_or_create_by_name(rfc)
end

#Seed Sanatoriums
{
    'Пермский край' => %w(Усть-Каспа Демидково Ключи Уральская\ венеция Алит Вита Красный\ Яр Жемчужина),
    'Удмуртия' => %w(Варзи-Ятчи Металлург Ува Чепца),
    'Башкирия' => %w(Янган-тау Юматово Танып Тау-Таш Зеленая\ роща Карагай Красноусольск),
    'Татария' => %w(Иж.Минводы Бакирово Жемчужина Ливадия),
    'Кировская область' => %w(Нижне-Ивкино Лесная\ Новь Митино Сосновый\ бор),
    'Свердловская область' => %w(Обуховский Самоцвет Нижние\ Серги),
    'Челябинская область' => %w(Увильды Кисегач Урал Юбилейный),
    'КавМинВоды' => %w(Ессентуки Железноводск Кисловодск Нальчик Пятигорск)
}.each do |region, sanatoriums|
  region = Region.find_by_name(region)
  if region
    sanatoriums.each do |s|
      Sanatorium.find_or_create_by_name(s, region_id: region.id)
    end
  end
end

#Seed Camp and Work America
%w(Camp\ USA Work\ and\ Travel).each do |camp|
  CampAndWork.find_or_create_by_name(camp)
end

#Seed Krasnodar region and cities
{
    'Краснодарский край' => %w(Сочи Анапа Геленджик Краснодар Тамань)
}.each do |region, cities|
  country = Country.find_by_name('Россия')
  region = Region.find_or_create_by_name(region, country_id: country.id)
  cities.each do |c|
    City.find_or_create_by_name_and_region_id(c, region.id)
  end
end








