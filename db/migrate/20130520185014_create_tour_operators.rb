class CreateTourOperators < ActiveRecord::Migration
  def change
    create_table :tour_operators do |t|
      t.integer :id
      t.string :name
      t.string :url
      t.string :service_url

      t.timestamps
    end

    add_index :tour_operators, :name, unique: true
    add_index :tour_operators, :url, unique: true
    add_index :tour_operators, :service_url, unique: true

  end
end
