class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :id
      t.string :email
      t.string :name
      t.string :subject
      t.text :text

      t.timestamps
    end
  end
end
