class CreateMotorShipsRiverCruisesTable < ActiveRecord::Migration

  def change
    create_table :motor_ships_river_cruises, id: false do |t|
      t.references :motor_ship, null: false
      t.references :river_cruise, null: false
    end
    add_index :motor_ships_river_cruises, :motor_ship_id
    add_index :motor_ships_river_cruises, :river_cruise_id
    add_index :motor_ships_river_cruises, [:motor_ship_id, :river_cruise_id], unique: true, name: 'by_motor_ship_river_cruise'
  end

end
