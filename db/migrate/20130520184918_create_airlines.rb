class CreateAirlines < ActiveRecord::Migration
  def change
    create_table :airlines do |t|
      t.integer :id
      t.integer :country_id
      t.integer :city_id
      t.integer :tour_operator_id

      t.timestamps
    end
  end
end
