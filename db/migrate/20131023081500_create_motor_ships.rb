class CreateMotorShips < ActiveRecord::Migration
  def change
    create_table :motor_ships do |t|
      t.text :name, null: false
      t.text :content
      t.integer :city_id
      t.text :price_file
      t.timestamps
    end
    add_index :motor_ships, :city_id
  end
end
