class CreateRestForChildren < ActiveRecord::Migration
  def change
    create_table :rest_for_children do |t|
      t.text :name, null: false
      t.text :url
      t.text :content
      t.text :image
      t.timestamps
    end
  end
end
