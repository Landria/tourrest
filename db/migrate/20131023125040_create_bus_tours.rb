class CreateBusTours < ActiveRecord::Migration
  def change
    create_table :bus_tours do |t|
      t.text :name, null: false
      t.text :content, null: false
      t.timestamps
    end
    add_index :bus_tours, :name
  end
end
