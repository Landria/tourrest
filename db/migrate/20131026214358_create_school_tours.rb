class CreateSchoolTours < ActiveRecord::Migration
  def change
    create_table :school_tours do |t|
      t.text :name, null: false
      t.text :link
      t.text :type, null: false
      t.text :content
      t.text :price_file
      t.timestamps
    end
    add_index :school_tours, :type
    add_index :school_tours, :name
  end
end
