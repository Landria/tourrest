class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.integer :id
      t.string :name
      t.string :code
      t.string :currency_code
      t.string :visa

      t.timestamps
    end

    add_index :countries, :name, unique: true
    add_index :countries, :code, unique: true
  end
end
