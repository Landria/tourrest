class AddCodeToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :code, :string
    add_index :pages, :code, :unique => true
  end

  def self.down
    remove_column :pages, :code
    remove_index :pages, :code
  end
end
