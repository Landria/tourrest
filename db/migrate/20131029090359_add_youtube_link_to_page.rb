class AddYoutubeLinkToPage < ActiveRecord::Migration
  def change
    add_column :pages, :youtube_link, :text
  end
end
