class AddVisaDescriptionToCountryDescription < ActiveRecord::Migration
  def self.up
    add_column :country_descriptions, :visa_description, :text
  end

  def self.down
    remove_column :country_descriptions, :visa_description
  end
end
