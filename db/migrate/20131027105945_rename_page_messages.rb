class RenamePageMessages < ActiveRecord::Migration
  def change
    p  = Page.find_by_code('message_us')
    p.update_column(:code, 'message') if p
  end
end
