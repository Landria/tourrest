class CreateCampAndWorks < ActiveRecord::Migration
  def change
    create_table :camp_and_works do |t|
      t.text :name, null: false
      t.text :content
      t.timestamps
    end
  end
end
