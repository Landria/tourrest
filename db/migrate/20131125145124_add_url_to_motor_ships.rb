class AddUrlToMotorShips < ActiveRecord::Migration
  def change
    add_column :motor_ships, :url, :string
  end
end
