class CreateSanatoria < ActiveRecord::Migration
  def change
    create_table :sanatoria do |t|
      t.text :name, null: false
      t.text :link
      t.references :region
      t.timestamps
    end
    add_index :sanatoria, :region_id
    add_index :sanatoria, :name
    add_index :sanatoria, :link, unique: true
  end
end
