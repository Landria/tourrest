class CreateKrasnodarTours < ActiveRecord::Migration
  def change
    create_table :krasnodar_tours do |t|
      t.integer :city_id, null: false
      t.text :preferred_time
      t.boolean :air
      t.boolean :train
      t.boolean :bus
      t.boolean :ferry
      t.text :link
      t.timestamps
    end
    add_index :krasnodar_tours, :city_id
  end
end
