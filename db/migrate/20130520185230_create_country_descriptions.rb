class CreateCountryDescriptions < ActiveRecord::Migration
  def change
    create_table :country_descriptions do |t|
      t.integer :id
      t.integer :country_id
      t.text :text

      t.timestamps
    end
  end
end
