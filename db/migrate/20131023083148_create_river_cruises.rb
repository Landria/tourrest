class CreateRiverCruises < ActiveRecord::Migration
  def change
    create_table :river_cruises do |t|
      t.text :name, null: false
      t.text :source_url
      t.timestamps
    end
    add_index :river_cruises, :name
  end
end
