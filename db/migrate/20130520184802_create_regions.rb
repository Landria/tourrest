class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.integer :id
      t.string :name
      t.integer :country_id

      t.timestamps
    end
    add_index :regions, :name, unique: true
  end
end
