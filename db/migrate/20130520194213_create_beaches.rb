class CreateBeaches < ActiveRecord::Migration
  def change
    create_table :beaches do |t|
      t.integer :id
      t.integer :country_id
      t.integer :month
      t.integer :t_air
      t.integer :t_water

      t.timestamps
    end

    add_index :beaches, %w(country_id month), unique: true
  end
end
