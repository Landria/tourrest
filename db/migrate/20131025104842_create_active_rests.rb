class CreateActiveRests < ActiveRecord::Migration
  def change
    create_table :active_rests do |t|
      t.text :name, null: false
      t.text :content
      t.text :price_file
      t.timestamps
    end
  end
end
