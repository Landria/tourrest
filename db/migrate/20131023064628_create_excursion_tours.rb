class CreateExcursionTours < ActiveRecord::Migration
  def change
    create_table :excursion_tours do |t|
      t.references :country, null: false
      t.text :preferred_time
      t.boolean :air
      t.boolean :train
      t.boolean :bus
      t.boolean :ferry
      t.timestamps
    end
    add_index :excursion_tours, :country_id, unique: true
  end
end
