class AddTourOperatorIdToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :tour_operator_id, :integer
  end
end
