class RemoveColumnImageFromRestForChildren < ActiveRecord::Migration
  def change
    remove_column :rest_for_children, :image
  end
end
