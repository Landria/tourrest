class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.integer :id
      t.string :name
      t.text :content, :limit => nil
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
