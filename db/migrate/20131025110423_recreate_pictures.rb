class RecreatePictures < ActiveRecord::Migration
  def change
    Picture.find_each { |p| p.image.recreate_versions! }
  end
end
