class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.integer :id
      t.string :name
      t.integer :region_id

      t.timestamps
    end

    add_index :cities, :name, unique: true
    add_index :cities, %w(name region_id), unique: true
  end
end
