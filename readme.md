#Настройка сервера
https://coderwall.com/p/yz8cha - неплохая инструкция

1. sudo useradd -d /home/tourrest -m tourrest
2. sudo passwd tourrest (echo 'loginname ALL=(ALL) ALL' >> /etc/sudoers)
3. sudo zypper install nginx
4. sudo zypper install postgresql postgresql-server
5. sudo zypper install make make-install gcc nano mc git postgresql-devel libxml2-devel libxml2 libxslt-devel
6. \curl -L https://get.rvm.io | bash -s stable --ruby
7. \curl -L https://get.rvm.io | bash -s -- --ignore-dotfiles
8. echo "source $HOME/.rvm/scripts/rvm" >> ~/.bash_profile
   или echo "source $HOME/.rvm/scripts/rvm" >> ~/.bashrc
9.  source ~/.rvm/scripts/rvm
10. rvm use 2.0 --default
11. rvm rubygems current
12. gem install rails --no-ri --no-rdoc
13. bundle install --path vendor/cache
14. /etc/init.d/nginx start
15. /etc/init.d/postgresql start
16. sudo zypper install imagemagick

#БД
1. su
2. su - postgres
3. psql -d template1
4. CREATE USER username WITH PASSWORD 'password' CREATEDB;
5. alter role username superuser createrole createdb replication;
6. create database projectname_production owner username;
7. sudo nano /var/lib/pgsql/data/pg_hba.conf (local to trust)
8. bundle exec rake RAILS_ENV=production  db:migrate db:seed # в current

#Генерация ключа ssh
1. ssh-keygen -t rsa -C "your_email@example.com"
2. ssh-add id_rsa
3. cat ~/.ssh/id_rsa.pub

#Резервное копирование
1. sudo gem install backup
2. backup generate:model -t tourrest_backup --archives --databases=postgresql,redis --compressors=gzip \--encryptors=gpg --storages=dropbox
3. Внести нужные данные в настройки (https://www.dropbox.com/developers/apps)
4. backup perform --trigger tourrest_backup

#Окружение
1. mkdir TourRest/releases
2. mkdir TourRest/shared/config
3. mkdir TourRest/shared/pids
4. mkdir TourRest/shared/log
5. mkdir TourRest/shared/uploads
6. nano TourRest/shared/config/database.yml # внести настройки бд
7. cap deploy
8. sudo ln -s /home/tourrest/TourRest/current/config/nginx/tourrest.conf /etc/nginx/vhosts.d/tourrest.conf
9. ls -l
10. cap deploy:restart