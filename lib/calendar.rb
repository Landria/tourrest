class Calendar

  MONTHS = {
      'Январь' => 1, 'Февраль' => 2, 'Март' => 3, 'Апрель' => 4,  'Май' => 5, 'Июнь' => 6,
      'Июль' => 7, 'Август' => 8, 'Сентябрь' => 9, 'Октябрь' => 10, 'Ноябрь' => 11, 'Декабрь' => 12
  }

  attr_accessor :current_month

  class << self

    def month_names
      MONTHS.keys
    end

    def month_numbers
      MONTHS.values
    end

    def month_number(month_name)
      MONTHS.values_at(month_name).first
    end

    def month_name(month_number)
      MONTHS.key(month_number)
    end

  end

  def initialize(month)
    month = month.present? ? month.to_i : Date.current.month
    self.current_month = month
  end

  def next
    current_month < 12 ? current_month + 1 : 1
  end

  def prev
    current_month > 1 ? current_month - 1 : 12
  end

end
