require 'net/http'
require 'uri'
require 'json'
module OpenWeatherMapApi
  class Client

    attr_accessor :json, :error, :lang, :data, :units, :cnt, :api_key, :mode, :result

    URL = "http://api.openweathermap.org/data/2.5/weather"
    CNT_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&lang=ru"
    ICON_URL = "http://openweathermap.org/img/w/"

    DEFAULT_LANG = :ru
    DEFAULT_MODE = :json
    DEFAULT_UNITS = 'metric'

    WIND_DIRECTION = {
        'N' => 0..0,
        'E' => 90..90,
        'S' => 180..180,
        'W' => 270..270,
        'NE' => 0..90,
        'NW' => 270..360,
        'SE' => 90..180,
        'SW' => 180..270
    }

    def initialize options = {}
      options.with_indifferent_access
      self.api_key = options[:api_key] if options[:api_key]
      self.lang = options[:lang] ? options[:lang] : DEFAULT_LANG
      self.units = options[:units] ? options[:units] : DEFAULT_UNITS
    end

    def forecast_by_name city_name, days=nil
      reset
      params = {:q => city_name}
      forecast params, days
    end

    def forecast_by_id city_id, days=nil
      reset
      params = {:id => city_id}
      forecast params, days
    end

    def error?
      !self.error.nil?
    end

    def ok?
      self.error.nil?
    end

    private

    def reset
      self.json = nil
      self.data = nil
      self.cnt = nil
    end

    def forecast params, days
      set_cnt days
      request_data params
      parse_data
      prepare_result
    end

    def set_cnt days
      self.cnt = days.abs <=14 ? days.abs : 14 if days
    end

    def wind_direction degree
      WIND_DIRECTION.each_pair do |dir, range|
        return dir if range.cover? degree
      end
    end

    def prepare_result
      self.result = []
      if self.cnt
        self.data["list"].each do |dt_forecast|
          populate_data dt_forecast
        end
      else
        populate_data
      end
      self.result
    rescue Exception => e
      handle_error __method__, e
      nil
    end

    def populate_data dt_forecast=nil
      self.result << {(self.cnt ? dt_forecast["dt"] : Date.today.to_time.to_i) => {
          description: (self.cnt ? dt_forecast["weather"][0]["description"].mb_chars.capitalize! : self.data["weather"][0]["description"].mb_chars.capitalize!),
          icon_path: (self.cnt ? "#{ICON_URL}#{dt_forecast["weather"][0]["icon"]}.png" : "#{ICON_URL}#{self.data["weather"][0]["icon"]}.png"),
          temp: (self.cnt ? dt_forecast["temp"]["day"] : self.data["main"]["temp"]),
          temp_min: (self.cnt ? dt_forecast["temp"]["min"] : self.data["main"]["temp_min"]),
          temp_max: (self.cnt ? dt_forecast["temp"]["max"] : self.data["main"]["temp_max"]),
          pressure: (self.cnt ? dt_forecast["pressure"] : self.data["main"]["pressure"]),
          humidity: (self.cnt ? dt_forecast["humidity"] : self.data["main"]["humidity"]),
          wind_speed: (self.cnt ? dt_forecast["speed"] : self.data["wind"]["speed"]),
          wind_direction: (self.cnt ? wind_direction(dt_forecast["deg"]) : wind_direction(self.data["wind"]["deg"]))
      }}
    end

    def request_data params
      params = params.merge lang: self.lang, units: self.units, mode: DEFAULT_MODE
      params = params.merge :APPID => self.api_key if self.api_key
      params = params.merge cnt: self.cnt if self.cnt

      uri = URI.parse(self.cnt ? CNT_URL : URL)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.path)
      request.set_form_data(params)
      request = Net::HTTP::Get.new(uri.path+ '?' + request.body)
      response = http.request(request)
      self.json = response.body
    rescue Exception => e
      handle_error __method__, e
    end

    def parse_data
      self.data = JSON.parse(self.json)
    rescue Exception => e
      handle_error __method__, e
    end

    def handle_error method, e
      self.error = "#{method}: #{e.message}"
      Rails.logger.info self.error
    end
  end
end