module Breadcrumbs

  extend ActiveSupport::Concern

  included do

    def self.add_breadcrumb(options = {})
      before_filter options do |controller|
        controller.add_breadcrumb(options)
      end
    end

  end

  def add_breadcrumb(options = {})
    page_key = options[:pluralize] ? controller_name : controller_name.singularize
    title = options[:title] || Settings.pages[page_key]
    @breadcrumbs ||= []
    content =
        if options[:path]
          path = options[:path].is_a?(Symbol) ? send(options[:path]) : options[:path]
          view_context.link_to_unless_current title, path do
            view_context.content_tag(:span, title)
          end
        else
          view_context.content_tag(:span, title)
        end
    @breadcrumbs << content
  end

end
