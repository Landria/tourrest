# encoding: utf-8

FactoryGirl.define do
  factory :beach do
    country_id 1
    month 7
    t_air 35
    t_water 38
  end
end
