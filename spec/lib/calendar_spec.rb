require 'spec_helper'

describe Calendar do
  it 'should return valid month_name' do
    described_class.month_name(1).should eq('Январь')
    described_class.month_name(2).should eq('Февраль')
    described_class.month_name(3).should eq('Март')
    described_class.month_name(4).should eq('Апрель')
    described_class.month_name(5).should eq('Май')
    described_class.month_name(6).should eq('Июнь')
    described_class.month_name(7).should eq('Июль')
    described_class.month_name(8).should eq('Август')
    described_class.month_name(9).should eq('Сентябрь')
    described_class.month_name(10).should eq('Октябрь')
    described_class.month_name(11).should eq('Ноябрь')
    described_class.month_name(12).should eq('Декабрь')
  end

end