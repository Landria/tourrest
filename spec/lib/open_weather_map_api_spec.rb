require 'spec_helper'

describe OpenWeatherMapApi::Client do
  let(:json) { {
      "coord" => {"lon" => 54.11, "lat" => 56.77},
      "sys" => {"message" => 0.0046, "country" => "Russia", "sunrise" => 1384920919, "sunset" => 1384948998},
      "weather" => [{"id" => 800, "main" => "Clear", "description" => "ясно", "icon" => "01d"}],
      "base" => "gdps stations",
      "main" => {"temp" => 0.19500000000005, "temp_min" => 0.19500000000005, "temp_max" => 0.19500000000005, "pressure" => 1026.64, "sea_level" => 1043.86, "grnd_level" => 1026.64, "humidity" => 66},
      "wind" => {"speed" => 6.09, "deg" => 243.001},
      "clouds" => {"all" => 0},
      "dt" => 1384947178,
      "id" => 569742,
      "name" => "Chaykovskiy",
      "cod" => 200
  }.to_json }
  let(:wrong_json) { {
      "coord" => {"lon" => 54.11, "lat" => 56.77},
      "sys" => {"message" => 0.0046, "country" => "Russia", "sunrise" => 1384920919, "sunset" => 1384948998},
      "weather" => [{"id" => 800, "main" => "Clear", "description" => "ясно", "icon" => "01d"}],
      "base" => "gdps stations",
      "main" => {"temp" => 0.19500000000005, "temp_min" => 0.19500000000005, "temp_max" => 0.19500000000005, "pressure" => 1026.64, "sea_level" => 1043.86, "grnd_level" => 1026.64, "humidity" => 66},
      "wind" => {"speed" => 6.09, "deg" => 243.001},
      "clouds" => {"all" => 0},
      "dt" => 1384947178,
      "id" => 569742,
      "name" => "Chaykovskiy",
      "cod" => 200
  } }
  let(:json_7) { '{"cod":"200","message":0.0089,"city":{"id":2643743,"name":"London","coord":{"lon":-0.12574,"lat":51.50853},"country":"GB","population":1000000},"cnt":7,"list":[{"dt":1384945200,"temp":{"day":5.8,"min":0.19,"max":5.8,"night":0.19,"eve":3.04,"morn":4.51},"pressure":1001.59,"humidity":98,"weather":[{"id":501,"main":"Rain","description":"дождь","icon":"10d"}],"speed":6.96,"deg":305,"clouds":76,"rain":10},{"dt":1385031600,"temp":{"day":6.62,"min":2.18,"max":6.62,"night":2.18,"eve":4.24,"morn":3.1},"pressure":1010.96,"humidity":95,"weather":[{"id":803,"main":"Clouds","description":"пасмурно","icon":"04d"}],"speed":8.05,"deg":37,"clouds":68},{"dt":1385118000,"temp":{"day":6.3,"min":1.09,"max":7.22,"night":3.71,"eve":6.19,"morn":1.09},"pressure":1020.71,"humidity":96,"weather":[{"id":801,"main":"Clouds","description":"облачно","icon":"02d"}],"speed":5.36,"deg":353,"clouds":12},{"dt":1385204400,"temp":{"day":6.42,"min":2.31,"max":6.42,"night":3.74,"eve":2.74,"morn":2.51},"pressure":1028.02,"humidity":94,"weather":[{"id":802,"main":"Clouds","description":"слегка облачно","icon":"03d"}],"speed":4.55,"deg":343,"clouds":44},{"dt":1385290800,"temp":{"day":4.88,"min":1.73,"max":5.16,"night":2.45,"eve":3.35,"morn":1.73},"pressure":1033.45,"humidity":88,"weather":[{"id":800,"main":"Clear","description":"ясно","icon":"01d"}],"speed":6.16,"deg":334,"clouds":0},{"dt":1385377200,"temp":{"day":8.92,"min":5.91,"max":8.92,"night":5.91,"eve":6.45,"morn":6.31},"pressure":1044.28,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"легкий дождь","icon":"10d"}],"speed":3.53,"deg":45,"clouds":82,"rain":0.25},{"dt":1385463600,"temp":{"day":8.07,"min":2.12,"max":8.07,"night":2.12,"eve":5.15,"morn":4.26},"pressure":1047.2,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"ясно","icon":"01d"}],"speed":4.52,"deg":64,"clouds":0}]}' }
  let(:json_14) { '{"cod":"200","message":0.0036,"city":{"id":2643743,"name":"London","coord":{"lon":-0.12574,"lat":51.50853},"country":"GB","population":1000000},"cnt":14,"list":[{"dt":1384945200,"temp":{"day":4.51,"min":1.9,"max":4.51,"night":2.29,"eve":1.9,"morn":4.51},"pressure":1002.69,"humidity":95,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":8.23,"deg":263,"clouds":92,"rain":7.75},{"dt":1385031600,"temp":{"day":5.32,"min":3.11,"max":5.32,"night":3.91,"eve":3.54,"morn":3.11},"pressure":1009.79,"humidity":95,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":8.2,"deg":45,"clouds":48,"rain":2.5},{"dt":1385118000,"temp":{"day":6.85,"min":2.75,"max":6.85,"night":3.78,"eve":4.26,"morn":2.75},"pressure":1020.1,"humidity":95,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":6.5,"deg":11,"clouds":0},{"dt":1385204400,"temp":{"day":7.99,"min":3.71,"max":8.23,"night":3.74,"eve":7.44,"morn":3.71},"pressure":1027.06,"humidity":90,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":5.3,"deg":14,"clouds":0},{"dt":1385290800,"temp":{"day":4.88,"min":1.73,"max":5.16,"night":2.45,"eve":3.35,"morn":1.73},"pressure":1033.45,"humidity":88,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":6.16,"deg":334,"clouds":0},{"dt":1385377200,"temp":{"day":8.92,"min":5.91,"max":8.92,"night":5.91,"eve":6.45,"morn":6.31},"pressure":1044.28,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":3.53,"deg":45,"clouds":82,"rain":0.25},{"dt":1385463600,"temp":{"day":8.07,"min":2.12,"max":8.07,"night":2.12,"eve":5.15,"morn":4.26},"pressure":1047.2,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":4.52,"deg":64,"clouds":0},{"dt":1385550000,"temp":{"day":5.87,"min":0.95,"max":5.87,"night":5.15,"eve":4.34,"morn":0.95},"pressure":1042.62,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":2.22,"deg":352,"clouds":79},{"dt":1385636400,"temp":{"day":8.95,"min":3.01,"max":8.95,"night":3.01,"eve":4.79,"morn":7.82},"pressure":1026.38,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":7.09,"deg":87,"clouds":95,"rain":1.21},{"dt":1385722800,"temp":{"day":6.09,"min":2.6,"max":6.09,"night":2.6,"eve":3.37,"morn":3.71},"pressure":1021.21,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":3.25,"deg":133,"clouds":83,"rain":0.72},{"dt":1385809200,"temp":{"day":5.82,"min":-0.85,"max":5.82,"night":-0.85,"eve":2.01,"morn":1.6},"pressure":1021.64,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":2.49,"deg":11,"clouds":0},{"dt":1385895600,"temp":{"day":6.74,"min":1.23,"max":6.74,"night":6.01,"eve":5.99,"morn":1.23},"pressure":1013.29,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":7.14,"deg":112,"clouds":12},{"dt":1385982000,"temp":{"day":8.39,"min":6.2,"max":8.54,"night":8.28,"eve":8.54,"morn":6.2},"pressure":1003.25,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":8.19,"deg":91,"clouds":100,"rain":2.19},{"dt":1386068400,"temp":{"day":8.76,"min":6.96,"max":8.76,"night":6.96,"eve":7.59,"morn":8.19},"pressure":998.16,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":3.08,"deg":36,"clouds":99,"rain":3.28}]}' }
  let(:city) {'Чайковский'}
  let(:city_id) {569742}

  context 'by_name' do
    it 'should return data for today by name' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/weather?APPID=5e647c2957c11e7ae405e5b2a334a601&lang=ru&q=%D0%A7%D0%B0%D0%B9%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9&mode=json&units=metric").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => json, :headers => {})


      w = OpenWeatherMapApi::Client.new :api_key => Settings.open_weather_map.api_key
      result = w.forecast_by_name city
      result.count.should eq 1
    end

    it 'should return data for cnt=7 by name' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7&lang=ru&mode=json&q=%D0%A7%D0%B0%D0%B9%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9&units=metric").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => json_7, :headers => {})

      w = OpenWeatherMapApi::Client.new
      result = w.forecast_by_name city, 7
      result.count.should eq 7
    end

    it 'should return data for cnt=14 by name' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=14&lang=ru&mode=json&q=%D0%A7%D0%B0%D0%B9%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9&units=metric").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => json_14, :headers => {})

      w = OpenWeatherMapApi::Client.new
      result = w.forecast_by_name city, 14
      result.count.should eq 14
    end
  end

  context 'by_id' do
    it 'should return data for today' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/weather?id=569742&lang=ru&mode=json&units=metric").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(:status => 200, :body => json, :headers => {})

      w = OpenWeatherMapApi::Client.new
      result = w.forecast_by_id city_id
      result.count.should eq 1
    end

    it 'should return data for cnt=7' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7&id=569742&lang=ru&mode=json&units=metric").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => json_7, :headers => {})

      w = OpenWeatherMapApi::Client.new
      result = w.forecast_by_id city_id, 7
      result.count.should eq 7
    end

    it 'should return data for cnt=14' do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=14&id=569742&lang=ru&mode=json&units=metric").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => json_14, :headers => {})


      w = OpenWeatherMapApi::Client.new
      result = w.forecast_by_id city_id, 14
      result.count.should eq 14
    end
  end

  context 'errors' do
    it "should return error when incorrect data received" do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/weather?id=569742&lang=ru&mode=json&units=metric").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(:status => 200, :body => wrong_json, :headers => {})

      w = OpenWeatherMapApi::Client.new
      w.forecast_by_id city_id
      w.should_not be_ok
      w.error.should_not be_nil
    end

    it "should rise error when initialize fails" do
      stub_request(:get, "http://api.openweathermap.org/data/2.5/weather?id=569742&lang=ru&mode=json&units=metric").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(:status => 200, :body => json, :headers => {})

      expect{w = OpenWeatherMapApi::Client.new "some not options hash string"}.to raise_error
    end
  end

  it 'should reset attributes when new request' do
    stub_request(:get, "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=14&id=569742&lang=ru&mode=json&units=metric").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => json_14, :headers => {})

    stub_request(:get, "http://api.openweathermap.org/data/2.5/weather?id=569742&lang=ru&mode=json&units=metric").
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => json, :headers => {})

    w = OpenWeatherMapApi::Client.new
    w.forecast_by_id city_id, 14
    result = w.forecast_by_id city_id
    w.cnt.should be_nil
    result.count.should eq 1
  end
end