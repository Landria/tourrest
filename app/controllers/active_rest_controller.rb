class ActiveRestController < ApplicationController

  layout 'wide'

  add_breadcrumb path: :active_rest_index_path

  def index
    @active_rest = ActiveRest.ordered
  end

  def show
    @active_rest = ActiveRest.find(params[:id])
    add_breadcrumb title: @active_rest.name, path: active_rest_path(@active_rest)
  end

end
