class PiligrimTourController < ApplicationController

  layout 'wide'
  PAGE_NAME = :piligrim_tour

  def index
    @page = Page.find_by_code(PAGE_NAME)
    render :template => "pages/show_wide"
  end

end
