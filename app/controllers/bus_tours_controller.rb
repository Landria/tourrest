class BusToursController < ApplicationController
  layout 'wide'
  add_breadcrumb path: :bus_tours_path

  def index
    @bus_tours = BusTour.ordered
  end

  def show
    @bus_tour = BusTour.find(params[:id])
    add_breadcrumb title: @bus_tour.name, path: bus_tour_path(@bus_tour)
  end

end
