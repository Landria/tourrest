class BeachRestController < ApplicationController

  layout 'wide'
  add_breadcrumb path: :beach_rest_index_path

  def index
    @calendar = Calendar.new(params[:month])
    @beaches = Beach.grouped_by_month(@calendar.current_month)

    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

end
