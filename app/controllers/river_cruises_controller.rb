class RiverCruisesController < ApplicationController
  layout 'wide'
  before_filter :load_page

  add_breadcrumb path: :river_cruises_path

  def index
    @river_cruise_cities = City.with_motor_ships
  end

  def show
    @river_cruise_city = City.find(params[:id])
    add_breadcrumb title: @river_cruise_city.name, path: river_cruise_path(@river_cruise_city)
  end

  def load_page
    @page = Page.find_by_code(:river_cruise)
  end
end
