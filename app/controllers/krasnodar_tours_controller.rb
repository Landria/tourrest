class KrasnodarToursController < ApplicationController

  layout 'wide'
  add_breadcrumb path: :krasnodar_tours_path

  def index
    @krasnodar_tours = KrasnodarTour.ordered
  end

end
