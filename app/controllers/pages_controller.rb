class PagesController < ApplicationController

  before_filter :load_page

  def service_page
    add_breadcrumb title: @page.name, path: service_page_path(@page.code)
    render :show
  end

  def simple_page
    add_breadcrumb title: @page.name, path: simple_page_path(@page.code)
    render :show
  end

  def visa_index
    @page = Page.find_by_code(:visa)
    @countries = Country.with_visa
    add_breadcrumb title: @page.name
  end

  def visa_show
    @page = Page.find_by_code(:visa)
    @country = Country.find(params[:id].to_i)
    add_breadcrumb title: @page.name, path: visa_page_path
    add_breadcrumb title: @country.name if @country
  end

  private

  def load_page
    @page = Page.find_by_code(params[:page_code])
  end

end