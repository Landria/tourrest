class CampAndWorksController < ApplicationController
  layout 'wide'
  add_breadcrumb path: :camp_and_works_path

  def index
    @camp_and_works = CampAndWork.ordered
  end

  def show
    @camp_and_work = CampAndWork.find(params[:id])
    add_breadcrumb title: @camp_and_work.name, path: @camp_and_work
  end
end
