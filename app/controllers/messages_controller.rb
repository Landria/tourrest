class MessagesController < ApplicationController

  add_breadcrumb path: :new_message_path

  def new
    @message = Message.new
  end

  def create
    add_breadcrumb title: 'Отправка сообщения'
    @message = Message.new(params[:message])
    if @message.save
      redirect_to root_path, notice: 'Сообщение успешно отправлено!'
    else
      render action: :new
    end
  end

end
