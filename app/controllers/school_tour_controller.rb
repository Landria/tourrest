class SchoolTourController < ApplicationController
  layout 'wide'

  add_breadcrumb title: I18n.t('activerecord.models.school_tour.other'), path: :school_tour_index_path

  def index
  end
end
