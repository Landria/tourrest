class SanatoriaController < ApplicationController

  layout 'wide'

  add_breadcrumb path: :sanatoria_path

  def index
    @sanatoria_regions = Sanatorium.regions_names
  end

  def show
    region = Region.find(params[:id])
    @sanatoria = region.sanatoria.ordered
    add_breadcrumb title: region.name, path: sanatorium_path(region.id)
  end
end
