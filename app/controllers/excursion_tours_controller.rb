class ExcursionToursController < ApplicationController
  layout 'wide'
  add_breadcrumb path: :excursion_tours_path

  def index
    @excursion_tours = ExcursionTour.ordered
  end
end
