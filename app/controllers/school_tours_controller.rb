class SchoolToursController < ApplicationController

  layout 'wide'

  before_filter :current_class
  add_breadcrumb title: I18n.t('activerecord.models.school_tour.other'), path: :school_tour_index_path

  def index
    @school_tours = @current_class.send(:ordered)
    add_breadcrumb path: @current_class
    render :index
  end

  def show
    @school_tour = @current_class.send(:find, params[:id])
    add_breadcrumb path: "#{controller_name}_path".to_sym
    add_breadcrumb title: @school_tour.name, path: @school_tour
    render :show
  end

  private

  def current_class
    @current_class ||= controller_name.classify.constantize
  end

end
