class MotorShipsController < ApplicationController

  layout 'wide'
  add_breadcrumb title: 'Речные круизы', path: :river_cruises_path

  def show
    @river_cruise_city = City.find(params[:river_cruise_id])
    @motor_ship = MotorShip.find(params[:id])
    add_breadcrumb title: @river_cruise_city.name, path: river_cruise_path(@river_cruise_city)
    add_breadcrumb title: @motor_ship.name
  end

end