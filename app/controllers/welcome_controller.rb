class WelcomeController < ApplicationController
  def index
    pages = Settings.pages
    @tiles = [
        { path: beach_rest_index_path, title: pages['beach_rest'] },
        { path: excursion_tours_path, title: pages['excursion_tour'] },
        { path: river_cruises_path, title: pages['river_cruise'] },
        { path: bus_tours_path, title: pages['bus_tour'] },
        { path: krasnodar_tours_path, title: 'Россия Краснодарский край' },
        { path: sanatoria_path, title: pages['sanatorium'] },
        { path: rest_for_children_path, title: pages['rest_for_children'] },
        { path: school_tour_index_path, title: pages['school_tour'] },
        { path: language_camps_path, title: pages['language_camp'] },
        { path: camp_and_works_path, title: pages['camp_and_work'] },
        { path: active_rest_index_path, title: pages['active_rest'] },
        { path: piligrim_tour_index_path, title: 'Паломнические туры' }
    ]
  end
end