class ApplicationController < ActionController::Base
  include Breadcrumbs

  protect_from_forgery
  before_filter :get_pages, :get_exchange_rates, :get_slides, :set_page, :weather_forecast

  def weather_forecast
    @w_cities = City.all.map { |c| c.name unless c.name.nil? }
    @current_city = params[:city_name] ? params[:city_name] : @w_cities.try(:first)
    @current_date = params[:date] ? params[:date].to_date.strftime("%d.%m.%Y") : Date.today.strftime("%d.%m.%Y")

    prepare_day_forecast get_forecast

    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  private

  def get_forecast
    if session[@current_city]
      w_data = session[@current_city]
    else
      w = OpenWeatherMapApi::Client.new api_key: Settings.open_weather_map.api_key
      w_data = w.forecast_by_name @current_city, 7
      session[@current_city] = w_data if w_data
      Rails.logger.info "WEATHER DATA: #{w_data.inspect}"
    end
    w_data
  end

  def prepare_day_forecast data
    if data
      @dates = []
      @w_data = nil
      data.each do |w_d|
        w_d.each_pair { |key, data|
          key_date = Time.at(key).to_date.strftime("%d.%m.%Y")
          @dates << key_date
          @w_data = data if key_date == @current_date
        }
      end
    end
  end

  def get_pages
    @pages = Page.all
  end

  def get_exchange_rates
    @rates = session[:rates]
    unless @rates
      rates = ExchangeRates.new
      if rates.ok?
        @rates = {usd: rates.get_value_by_char_code('USD'), eur: rates.get_value_by_char_code('EUR'), date: rates.date, source: rates.source}
        session[:rates] = @rates
      end
    end
  end

  def get_slides
    @slides = News.active
  end

  def set_page
    @page ||= Page.find_all_by_code([controller_name, controller_name.singularize]).first
  end

end
