class RestForChildrenController < ApplicationController

  layout 'wide'
  add_breadcrumb path: :rest_for_children_path, pluralize: true

  def index
    @rest_for_children = RestForChildren.ordered
    @pictures = []
    @rest_for_children.each do |rest|
      if rest.pictures.any?
        rest.pictures.each do |pic|
          @pictures << pic
        end
      end
    end
  end
end
