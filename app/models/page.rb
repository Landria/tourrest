class Page < ActiveRecord::Base

  YOUTUBE_REGEXP = /\Ahttp:\/\/www.youtube.com\/watch\?v=\S+\z/

  attr_accessible :content, :description, :name, :title, :keywords, :code, :youtube_link, :pictures_attributes

  has_many :pictures, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :pictures, allow_destroy: true

  validates :content, :code, :name, presence: true, length: { minimum: 4 }
  validates :code, uniqueness: true

  validate :youtube_links, if: 'youtube_link.present? && youtube_link_changed?'

  def youtube_video_ids
    youtube_link.present? ? youtube_link.scan(/http:\/\/www.youtube.com\/watch\?v=([\w-]+)/).flatten : []
  end

  private

  def youtube_links
    youtube_link.split(',').each do |link|
      unless link.strip.match(YOUTUBE_REGEXP)
        errors.add(:youtube_link, "Ссылка не подходит: #{link}")
      end
    end
  end

end
