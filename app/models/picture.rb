class Picture < ActiveRecord::Base
  attr_accessible :image, :image_cache

  mount_uploader :image, PictureUploader

  belongs_to :imageable, polymorphic: true
end
