class RestForChildren < ActiveRecord::Base
  attr_accessible :content, :name, :url, :pictures_attributes

  validates :name, presence: true

  validates :url,
              format: { with: TourOperator::URL_REGEXP },
              uniqueness: true,
              if: 'url.present?'

  has_many :pictures, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :pictures, allow_destroy: true

  scope :ordered, -> { order(:name) }

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
