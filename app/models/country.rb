class Country < ActiveRecord::Base

  VISA_TYPES = %w(да нет на\ границе)

  attr_accessible :id, :name, :code, :currency_code, :visa, :tour_operator_id

  validates :visa, inclusion: { in: VISA_TYPES }
  validates :name, :code, :visa, presence: true

  validates :currency_code, length: (3..3), allow_blank: true
  validates :code, uniqueness: true, length: (2..2)

  has_many :airlines
  belongs_to :tour_operator
  has_many :country_descriptions

  default_scope -> { order(:name) }
  scope :with_visa, -> { where('visa != ?', VISA_TYPES[1]).order('countries.name') }

  def last_tour_operator
    if airlines.any?
      airlines.last.tour_operator
    end
  end

  def specify_airlines
    city_ids = City.select('id').where(name: %w(Москва Пермь Уфа)).to_a.map(&:id)
    al = airlines.includes(:city, :tour_operator).where('cities.id' => city_ids).order('tour_operators.name').to_a.group_by(&:city_id)
    return [] unless city_ids

    city_ids.map do |c_id|
      al[c_id] ? al[c_id].map {|a| {name: "#{a.tour_operator.name}", service_url: a.tour_operator.service_url }} : [{ name: '-' }]
    end
  end

  def visa_description
    country_descriptions.last.visa_description.html_safe
  rescue
  end

end
