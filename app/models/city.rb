class City < ActiveRecord::Base
  attr_accessible :id, :name, :region_id, :country_id

  belongs_to :region
  belongs_to :country

  has_many :motor_ships
  default_scope order(:name)

  validate :at_least_one_geo_param
  validates :name, presence: true

  def self.with_motor_ships
    City.select('cities.*').joins(:motor_ships).group('cities.id').having('count(motor_ships.id) > 0')
  end

  def at_least_one_geo_param
    if [self.region_id, self.country_id].compact.size == 0
      errors[:region_id] << ("Город должен принадлежать региону или стране.")
      errors[:country_id] << ("Город должен принадлежать региону или стране.")
    end
  end
end
