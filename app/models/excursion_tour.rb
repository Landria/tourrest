class ExcursionTour < ActiveRecord::Base
  attr_accessible :country_id, :preferred_time, :air, :train, :bus, :ferry
  belongs_to :country

  validates :country_id, presence: true, uniqueness: true
  scope :ordered, -> { includes(:country).order('countries.name') }
end
