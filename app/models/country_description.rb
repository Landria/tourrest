class CountryDescription < ActiveRecord::Base
  attr_accessible :id, :country_id, :text, :visa_description
  belongs_to :country
  validates :text, presence: true
  validates :country_id, presence: true
end
