class Message < ActiveRecord::Base
  attr_accessible :email, :name, :subject, :text

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  validates :email, :name, :subject, :text, presence: true

  validates :email, format: {with: EMAIL_REGEXP}, if: 'email.present?'

  after_create :send_email_to_admin

  scope :ordered, -> { order('created_at DESC') }

  private

  def send_email_to_admin
    TourrestMailer.send_email_to_admin(self).deliver
  end
end
