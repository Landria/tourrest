class Airline < ActiveRecord::Base
  attr_accessible :id, :country_id, :city_id, :tour_operator_id

  belongs_to :tour_operator
  belongs_to :country
  belongs_to :city

  validates :country_id, :city_id, :tour_operator_id, presence: true
  validates :tour_operator_id, uniqueness: { scope: [:country_id, :city_id] }

end
