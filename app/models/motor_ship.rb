class MotorShip < ActiveRecord::Base

  has_many :pictures, as: :imageable, dependent: :destroy
  attr_accessible :city_id, :content, :name, :price_file, :price_file_cache, :url, :pictures_attributes
  accepts_nested_attributes_for :pictures, allow_destroy: true
  belongs_to :city

  mount_uploader :price_file, FileUploader

  validates :name, presence: true

  scope :ordered, -> { order(:name) }

  def to_param
    "#{id}-#{name.parameterize}"
  end

  def self.for_city id
    where(city_id: id).ordered
  end

end
