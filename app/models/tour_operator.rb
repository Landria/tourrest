class TourOperator < ActiveRecord::Base
  attr_accessible :id, :name, :url, :service_url

  URL_REGEXP = /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?\Z/ix

  has_many :airlineses

  validates :name, :url, :service_url, presence: true

  validates :url,
            format: {with: URL_REGEXP},
            uniqueness: true,
            if: 'url.present?'

  validates :service_url,
            format: {with: URL_REGEXP},
            uniqueness: true,
            if: 'service_url.present?'

  scope :ordered, -> { order(:name) }

end
