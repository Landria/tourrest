class CampAndWork < ActiveRecord::Base
  attr_accessible :content, :name
  validates :name, presence: true

  scope :ordered, -> { order(:name) }

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
