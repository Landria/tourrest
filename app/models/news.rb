class News < ActiveRecord::Base
  mount_uploader :file, NewsUploader

  attr_accessible :file, :text, :title, :active, :file_cache

  validates :file, :title, presence: true

  scope :active, ->{ where(active: true) }
end
