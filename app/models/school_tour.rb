class SchoolTour < ActiveRecord::Base

  attr_accessible :content, :name, :price_file, :price_file_cache, :link

  validates :name, presence: true
  validates :link,
            format: { with: TourOperator::URL_REGEXP },
            uniqueness: true,
            if: 'link.present?'

  mount_uploader :price_file, FileUploader

  scope :ordered, -> { order(:name) }

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
