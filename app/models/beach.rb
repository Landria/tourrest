class Beach < ActiveRecord::Base
  attr_accessible  :id, :country_id, :month, :t_air, :t_water

  belongs_to :country, dependent: :destroy

  validates :country_id, :month,  presence: true
  validates :t_air, :t_water,     inclusion: { in: -50..50 }
  validates :month,               inclusion: { in: 1..12 }, uniqueness: { scope: [:country_id, :month] }

  def self.grouped_by_month(month)
    includes(:country).where(month: month).order('countries.name').to_a.group_by(&:month)
  end

end
