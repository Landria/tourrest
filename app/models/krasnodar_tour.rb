class KrasnodarTour < ActiveRecord::Base
  attr_accessible :air, :bus, :city_id, :ferry, :preferred_time, :train, :link

  validates :city_id, presence: true, uniqueness: true
  validates :link,
            format: {with: TourOperator::URL_REGEXP},
            uniqueness: true,
            if: 'link.present?'

  belongs_to :city

  scope :ordered, -> { includes(:city).order('cities.name') }
end
