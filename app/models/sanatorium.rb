class Sanatorium < ActiveRecord::Base
  attr_accessible :link, :name, :region_id
  belongs_to :region

  validates :name, presence: true
  validates :region_id, presence: true
  validates :link,
            format: {with: TourOperator::URL_REGEXP},
            uniqueness: true,
            presence: true

  scope :ordered, -> { order(:name) }

  def self.regions_names
    includes(:region).order('regions.name').to_a.map{|s| { id: s.region.id, name: s.region.name} unless s.region.nil?}.uniq
  end

end
