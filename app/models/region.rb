class Region < ActiveRecord::Base

  attr_accessible  :id, :country_id, :name
  belongs_to :country
  has_many :sanatoria
  validates :country_id, :name, presence: true

  scope :ordered, -> { order(:name) }

end
