class ActiveRest < ActiveRecord::Base

  attr_accessible :content, :name, :price_file, :price_file_cache, :pictures_attributes
  has_many :pictures, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :pictures, allow_destroy: true

  scope :ordered, -> { order(:name) }

  validates :name, presence: true

  mount_uploader :price_file, FileUploader

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
