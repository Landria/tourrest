class BusTour < ActiveRecord::Base
  attr_accessible :content, :name, :pictures_attributes
  validates :name, :content, presence: true

  has_many :pictures, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :pictures, allow_destroy: true

  scope :ordered, -> { order(:name) }

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
