ActiveAdmin.register Region do
  menu parent: 'Справочники'

  filter :name
  filter :country

  index do
    column :name
    column :country
    default_actions
  end
end
