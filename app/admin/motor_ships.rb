ActiveAdmin.register MotorShip do

  menu parent: 'Виды отдыха', :label => "Речные круизы"

  form do |f|
    f.inputs do
      f.input :name, as: :string
      f.input :content
      f.input :city_id, as: :select, collection: City.scoped
      f.input :price_file, as: :file
      f.input :price_file_cache, as: :hidden
      f.input :url, :label => "Вншняя Ссылка"
    end

    f.inputs 'Фото' do
      f.has_many :pictures, allow_destroy: true do |pf|
        pf.input :image, as: :file
        pf.input :image_cache, as: :hidden
        pf.input :_destroy, as: :boolean, label: 'Удалить изображение'
        #NOTE Костыль
        pf.form_buffers.last << (image_tag(pf.object.image_url(:admin_thumb), style: 'padding: 5px;') if pf.object.image.present?)
      end
    end

    f.actions
  end

  filter :name
  filter :content
  filter :city

  index do
    column :name
    column :content do |ms|
      ms.content.truncate(200).html_safe
    end
    column :city
    default_actions
  end

  show do |msh|
    attributes_table do
      row :name
      row :content
      row :pictures do
        if msh.pictures.any?
          msh.pictures.each do |p|
            if p.image_url.present?
              div class: 'admin-photo' do
                image_tag(p.image_url(:admin_thumb))
              end
            end
          end
        end
      end
    end
  end
end
