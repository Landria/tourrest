ActiveAdmin.register CountryDescription do
  menu parent: 'Справочники'

  filter :country

  index do
    column :country
    column :text do |cd|
      cd.text.truncate(500).html_safe
    end
    default_actions
  end

  form do |f|
    f.inputs 'Описание страны' do
      f.input :country
      f.input :text
    end

    f.inputs 'Информация по визе' do
      f.input :visa_description
    end

    f.actions
  end

  show  do |c_d|
    attributes_table do
      row :country
      row 'Описание страны' do
        c_d.text.html_safe
      end
      row 'Информация по визе' do
        c_d.visa_description.html_safe
      end
      row :created_at
      row :updated_at
    end
  end
end
