ActiveAdmin.register City do
  menu parent: 'Справочники'

  filter :name
  filter :region
  filter :country
  controller do
    #def permitted_params
    #  params.permit(:asterisk_sip_conf_template => [:title, :name, :accountcode, :amaflags, :callgroup, :canreinvite, :defaultip, :dtmfmode, :fromuser,
    #                                                :fromdomain, :host, :insecure, :language, :mailbox, :md5secret, :nat, :permit, :deny, :mask, :pickupgroup, :port,
    #                                                :qualify, :restrictcid, :rtptimeout, :rtpholdtimeout, :type, :username, :allow, :musiconhold, :regseconds, :ipaddr,
    #                                                :regexten, :cancallforward, :lastms, :defaultuser, :fullcontact, :regserver, :useragent, :callbackextension])
    #end
  end

  index do
    column :name
    column :region
    column :country
    default_actions
  end

end
