ActiveAdmin.register News do

  form(html: {multipart: true}) do |f|
    f.inputs 'Основная часть' do
      f.input :title
      f.input :text, as: :text
      f.input :active
      f.input :file, as: :file
      f.input :file_cache, as: :hidden
    end
    f.actions
  end

  show do |n|
    attributes_table do
      row :title
      row :text
      row :active do
        n.active ? 'Отображается в слайдере' : 'Не отображается в слайдере'
      end
      row :file do
        image_tag(n.file_url) if n.file_url.present?
      end
      row :created_at
    end
  end
end
