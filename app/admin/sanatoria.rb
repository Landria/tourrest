ActiveAdmin.register Sanatorium do
  menu parent: 'Виды отдыха'

  form do |f|
    f.inputs 'Санаторий' do
      f.input :name, as: :string
      f.input :link, as: :string
      f.input :region_id, as: :select, collection: Region.ordered
    end
    f.actions
  end

  filter :name
  filter :link
  filter :region

  index do
    column :name
    column :region
    column :link do |s|
      link_to_if s.link.present?, s.link, s.link, target: '_blank'
    end
    default_actions
  end

end