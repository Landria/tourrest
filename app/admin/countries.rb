ActiveAdmin.register Country do

  menu parent: 'Справочники'

  form do |f|
    f.inputs 'Новая страна' do
      f.input :name
      f.input :code
      f.input :currency_code
      f.input :visa, as: :select, collection: Country::VISA_TYPES
      f.input :tour_operator_id, as: :select, collection: TourOperator.ordered
    end
    f.actions
  end

  filter :name
  filter :code
  filter :currency_code
  filter :visa
  filter :tour_operator

  index do
    column :name
    column :code
    column :currency_code
    column :visa
    column :tour_operator
    default_actions
  end


end
