ActiveAdmin.register_page 'Dashboard' do

  menu priority: 1, label: 'Главная панель'

  content title: proc { I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel 'Последние сообщения' do
          table_for Message.ordered.limit(5) do
            column :email
            column :name
            column :subject
            column :created_at
          end
          strong { link_to 'Все сообщения', admin_messages_path }
        end
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
