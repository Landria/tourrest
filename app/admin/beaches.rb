ActiveAdmin.register Beach do
  menu parent: 'Виды отдыха'

  index do
    column :id
    column 'Страна' do |rest|
      rest.country.try(:name)
    end
    column 'Месяц' do |rest|
      I18n.t("months.#{Date::MONTHNAMES[rest.month]}")
    end
    column :t_water
    column :t_air
    default_actions
  end

  form do |f|
    f.inputs 'Направление пляжного отдыха' do
      f.input :country_id, as: :select, collection: Country.scoped
      f.input :month, as: :select, collection: Calendar::MONTHS
      f.input :t_water
      f.input :t_air
    end
    f.actions
  end
end