ActiveAdmin.register Page, as: 'Content' do
  menu label: 'Страницы'
  config.batch_actions = false

  index as: :blog do
    title do |page|
      "<p>#{page.name}: <span class='meta'>#{page.code}</span></p>".html_safe
    end
    body do |page|
      "<p>#{truncate(page.content, length: 100)}</p>".html_safe
    end
  end

  form do |f|
    f.inputs 'Основная часть' do
      f.input :name
      f.input :content
      f.input :youtube_link, as: :string, hint: 'Например: http://www.youtube.com/watch?v=fxGxc-HPnco. Несколько видео можно добавить, разделяя ссылки через запятую'
    end

    f.inputs 'Meta-данные' do
      f.input :title
      f.input :description
      f.input :keywords
    end

    f.inputs 'Фото' do
      f.has_many :pictures, allow_destroy: true do |pf|
        pf.input :image, as: :file
        pf.input :image_cache, as: :hidden
        pf.input :_destroy, as: :boolean, label: 'Удалить изображение'
        #NOTE Костыль
        pf.form_buffers.last << (image_tag(pf.object.image_url(:admin_thumb), style: 'padding: 5px;') if pf.object.image.present?)
      end
    end

    f.actions
  end

  show do |page|
    attributes_table do
      row :name
      row :code
      row :content
      row :youtube_link
      row :pictures do
        if page.pictures.any?
          page.pictures.each do |p|
            if p.image_url.present?
              div class: 'admin-photo' do
                image_tag(p.image_url(:admin_thumb))
              end
            end
          end
        end
      end
      row :title
      row :description
      row :keywords
    end
  end

end
