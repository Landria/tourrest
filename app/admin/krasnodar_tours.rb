ActiveAdmin.register KrasnodarTour do
  menu parent: 'Виды отдыха'

  form do |f|
    f.inputs 'Россия Краснодарский край' do
      f.input :city_id, as: :select, collection: City.find_all_by_region_id(Region.find_by_name('Краснодарский край'))
      f.input :preferred_time, as: :string
      f.input :link, as: :string
      f.input :air
      f.input :train
      f.input :bus
      f.input :ferry
    end
    f.actions
  end

  filter :city
  filter :preferred_time
  filter :air
  filter :train
  filter :bus
  filter :ferry
  filter :link

end