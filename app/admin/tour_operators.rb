ActiveAdmin.register TourOperator do
  menu parent: 'Справочники'

  filter :name
  filter :url
  filter :service_url

  index do
    column :name
    column :url do |to|
     link_to_if to.url.present?, to.url, to.url, target: '_blank'
    end
    column :service_url do |to|
      link_to_if to.url.present?, to.url, to.url, target: '_blank'
    end
    default_actions
  end


end
