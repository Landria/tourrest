ActiveAdmin.register RestForChildren do

  menu parent: 'Виды отдыха'

  form do |f|
    f.inputs 'Детский отдых' do
      f.input :name, as: :string
      f.input :url, as: :string
      f.input :content
    end

    f.inputs 'Фото' do
      f.has_many :pictures, allow_destroy: true do |pf|
        pf.input :image, as: :file
        pf.input :image_cache, as: :hidden
        pf.input :_destroy, as: :boolean, label: 'Удалить изображение'
        #NOTE Костыль
        pf.form_buffers.last << (image_tag(pf.object.image_url(:admin_thumb), style: 'padding: 5px;') if pf.object.image.present?)
      end
    end

    f.actions
  end

  show do |rfc|
    attributes_table do
      row :name
      row :url do
        link_to rfc.url, rfc.url, target: '_blank' if rfc.url
      end
      row :pictures do
        if rfc.pictures.any?
          rfc.pictures.each do |p|
            if p.image_url.present?
              div style: 'float: left; padding: 5px;' do
                image_tag(p.image_url(:admin_thumb))
              end
            end
          end
        end
      end
    end
  end

  index do
    column :name
    column :url do |rfc|
      link_to rfc.url, rfc.url, target: '_blank' if rfc.url
    end
    default_actions
  end

end
