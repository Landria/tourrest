ActiveAdmin.register Airline do
  menu parent: 'Справочники'

  index do
    column :tour_operator
    column :country
    column :city
    default_actions
  end

  filter :tour_operator
  filter :country
  filter :city

end
