ActiveAdmin.register CampAndWork do

  menu parent: 'Виды отдыха'

  form do |f|
    f.inputs 'Camp and Work America' do
      f.input :name, as: :string
      f.input :content
    end
    f.actions
  end

  filter :name

  index do
    column :name
    default_actions
  end

end
