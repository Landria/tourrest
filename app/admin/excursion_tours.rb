ActiveAdmin.register ExcursionTour do
  menu parent: 'Виды отдыха'

  index do
    column :id
    column :country
    column :preferred_time
    column :air do |rest|
      rest.air ? "Да" : "Нет"
    end
    column :train do |rest|
      rest.train ? "Да" : "Нет"
    end
    column :bus do |rest|
      rest.bus ? "Да" : "Нет"
    end
    column :ferry do |rest|
      rest.ferry ? "Да" : "Нет"
    end
    column :created_at
    default_actions
  end

  form do |f|
    f.inputs 'Экскурсионный тур' do
      f.input :country_id, as: :select, collection: Country.scoped
      f.input :preferred_time, as: :string
      f.input :air
      f.input :train
      f.input :bus
      f.input :ferry
    end
    f.actions
  end
end