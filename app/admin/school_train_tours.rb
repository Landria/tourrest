ActiveAdmin.register SchoolTrainTour do

  menu parent: 'Школьные туры'

  form do |f|
    f.inputs 'Школьный Ж/д тур' do
      f.input :name, as: :string
      f.input :content
      f.input :link, as: :string
      f.input :price_file, as: :file
      f.input :price_file_cache, as: :hidden
    end
    f.actions
  end

  filter :name
  filter :content
  filter :link

  index do
    column :name
    column :link do |st|
      link_to_if st.link.present?, st.link, st.link, target: '_blank'
    end
    default_actions
  end

  show do |st|
    attributes_table do
      row :name
      row :content do
        st.content.try(:truncate, 500)
      end
      row :link do
        link_to_if st.link.present?, st.link, st.link, target: '_blank'
      end
      row :price_file
      row :created_at
      row :updated_at
    end
  end

end