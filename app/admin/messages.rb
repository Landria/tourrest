ActiveAdmin.register Message  do
  menu
  actions :index, :show, :update
  show do |ad|
    attributes_table do
      row :email
      row :name
      row :subject
      row :text
      row :created_at

    end
  end
end
