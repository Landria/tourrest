# encoding: utf-8

class NewsUploader < CarrierWave::Uploader::Base

  permissions 0644

  # Include RMagick or MiniMagick support:
  #include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  include Sprockets::Helpers::RailsHelper
  include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  storage :file

  def store_dir
    'uploads/news-slides'
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  process :resize_to_fill => [235, 235]

  #version :thumb do
  #  process :resize_to_fill => [100, 100]
  #end

=begin
  def move_to_cache
    true
  end

  def move_to_store
    true
  end
=end

end
