module ApplicationHelper

  def title(title = nil)
    if title.present?
      content_for :title, title
    else
      content_for?(:title) ? Settings.default_title + ' | ' + content_for(:title) : Settings.default_title
    end
  end

  def meta_keywords(tags = nil)
    if tags.present?
      content_for :meta_keywords, tags
    else
      content_for?(:meta_keywords) ? [content_for(:meta_keywords), Settings.meta_keywords].join(', ') : Settings.meta_keywords
    end
  end

  def meta_description(desc = nil)
    if desc.present?
      content_for :meta_description, desc
    else
      content_for?(:meta_description) ? content_for(:meta_description) : Settings.meta_description
    end
  end

  def airlines_links(airlines)
    airlines.map do |a|
      link_to_if a[:service_url], '+', a[:service_url], target: '_blank'
    end.join('<br>')
  end

  def human_boolean(val)
    val ? '+' : '-'
  end

  def youtubify(video_ids)
    video_ids.map do |v_id|
      content_tag(:div, content_tag(:iframe, nil,
                  src: "//www.youtube.com/embed/#{v_id}?rel=0",
                  width: 695, height: 400,
                  frameborder: 0, allowfulscreen: true
      ))
    end.join('')
  end

end
