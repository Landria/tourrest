class TourrestMailer < ActionMailer::Base
  default from: Devise.mailer_sender

  def send_email_to_admin(message)
    @message = message

    AdminUser.find_each do |admin|
      mail to: admin.email,
           from: @message.email,
           subject: "Новое сообщение с сайта: #{@message.subject}"
    end
  end

end
