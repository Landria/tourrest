//= require active_admin/base
//= require nicEdit

$( document ).ready(function() {
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
});
