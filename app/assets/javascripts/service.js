$(document).ready(function () {
    jQuery.ajaxSetup({
        'beforeSend': function (xhr) {xhr.setRequestHeader("Accept", "text/javascript")}
    });

    $("#news-slides").slides({
        container:'news-slides',
        generateNextPrev:false,
        pagination:true,
        generatePagination:true,
        paginationClass:'list-hr',
        play:5000,
        pause:2500
    });

    $("#news-slides ul li a").wrapInner("<span></span>");

    $("#media-slides").slides({
        container:'media-slides',
        generateNextPrev:false,
        pagination:true,
        generatePagination:true,
        paginationClass:'list-hr',
        play:6000,
        pause:2000
    });

    $("#media-slides ul li a").wrapInner("<span></span>");

    $("#partners-logos").carouFredSel({
        items:5,
        circular:false,
        infinite:false,
        auto:false,
        scroll:{
            items:1,
            duration:500,
            pauseOnHover:true
        },
        prev:{
            button:"#partners-logos-prev"
        },
        next:{
            button:"#partners-logos-next"
        }
    });
});