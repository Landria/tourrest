Tourrest::Application.routes.draw do
  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  root to: 'welcome#index'

  scope controller: :pages do
    get 'service/:page_code', to: :service_page,  as: :service_page, constraints: { page_code: /#{Settings.service_pages.join('|')}/ }
    get '/:page_code', to: :simple_page,  as: :simple_page, constraints: { page_code: /#{Settings.simple_pages.join('|')}/ }
    #get '/rest/:page_code', to: :simple_page,  as: :simple_page, constraints: { page_code: /#{Settings.simple_pages.join('|')}/ }
    get '/countries/visa' => 'pages#visa_index', as: :visa_page
    get '/countries/visa/:id' => 'pages#visa_show', as: :visa_show_page
  end
  scope path: 'rest' do

    resources :beach_rest, only: [:index], path: 'beach' do
      get 'month/:month', to: :index, on: :collection, as: :by_month, constraints: {month: /[1-9]|1[0-2]/}
    end

    resources :excursion_tours, only: :index
    resources :river_cruises, only: [:show, :index] do
      resources :motor_ships, only: [:show]
    end
    resources :bus_tours, only: [:show, :index]
    resources :active_rest, only: [:show, :index]
    resources :rest_for_children, only: [:index]
    resources :language_camps, only: [:index]

    resources :school_bus_tours, only: [:show, :index]
    resources :school_train_tours, only: [:show, :index]
    resources :school_overseas_tours, only: [:show, :index]
    resources :sanatoria, only: [:show, :index]
    resources :camp_and_works, only: [:show, :index]
    resources :krasnodar_tours, only: [:index]

    resources :school_tour, only: [:index]
    resources :piligrim_tour, only: [:index]
    #scope controller: :pages do
    #  get :school_tours
    #end

  end

  resources :messages, only: [:new, :create]

  match "/weather_forecast" => 'application#weather_forecast', :constraints => { :only_ajax => true }

  #match '/rest/beach' => 'beach_rest#index', via: [:get], :as => :beach_rest

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
